package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		System.out.println("Start application");
		System.out.println("Another message");
		System.out.println("Second message");
		System.out.println(new Date());
		SpringApplication.run(DemoApplication.class, args);
	}

}
